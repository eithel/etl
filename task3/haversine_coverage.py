from math import sin, cos, sqrt, atan2, radians

# approximate radius of earth in km
R = 6373.0


def haversine(lat1, lng1, lat2, lng2):
    """haversine calculate the distance between two points in km"""

    lat1 = radians(lat1)
    lng1 = radians(lng1)
    lat2 = radians(lat2)
    lng2 = radians(lng2)
    
    dlon = lng2 - lng1
    dlat = lat2 - lat1
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    return R * c


def map_coverage(s):
    """map_coverage returns for a shopper, the coverage respect locations"""
    ris = filter(lambda v: v < 10, map(lambda l: haversine(
        s['lat'], s['lng'], l['lat'], l['lng']), locations))
    return {'shopper_id': s['id'], 'coverage': len(list(ris))/len(locations)*100}

locations = [
    {'id': 1000, 'zip_code': '37069', 'lat': 45.35, 'lng': 10.84},
    {'id': 1001, 'zip_code': '37121', 'lat': 45.44, 'lng': 10.99},
    {'id': 1001, 'zip_code': '37129', 'lat': 45.44, 'lng': 11.00},
    {'id': 1001, 'zip_code': '37133', 'lat': 45.43, 'lng': 11.02},
]

shoppers = [
    {'id': 'S1', 'lat': 45.46, 'lng': 11.03, 'enabled': True},
    {'id': 'S2', 'lat': 45.46, 'lng': 10.12, 'enabled': True},
    {'id': 'S3', 'lat': 45.34, 'lng': 10.81, 'enabled': True},
    {'id': 'S4', 'lat': 45.76, 'lng': 10.57, 'enabled': True},
    {'id': 'S5', 'lat': 45.34, 'lng': 10.63, 'enabled': False},
    {'id': 'S6', 'lat': 45.42, 'lng': 10.81, 'enabled': True},
    {'id': 'S7', 'lat': 45.34, 'lng': 10.94, 'enabled': True},
]


# only active shopper
active_shoppers = list(filter(lambda s: s['enabled'], shoppers))

# sort all the shoppers by coverage
sort = sorted(map(map_coverage, active_shoppers),
             key=lambda x: x['coverage'], reverse=True)

print(list(sort))
