<?php


namespace Task2;


/**
 * Class Path helps to navigate filesystem
 */
class Path {
	/**
	 * @var string The current path
	 */
	protected $currentPath = '';

	/**
	 * Path constructor.
	 * Receive a string as path and save the initial path
	 *
	 * @param string $start
	 */
	public function __construct(string $start)
	{
		$this->currentPath = $this
			->cd($start)
			->getCurrentPath();
	}

	/**
	 * Given a new path, navigate the filesystem
	 *
	 * @param string $newPath An always valid path
	 *
	 * @return Path Return the object itself, so we can chain multiple navigation
	 */
	public function cd(string $newPath): self
	{
		// We use the currentPath as start in order to remove / add path parts.
		// We use array filter to reset array with empty values.
		$tmpPath = array_filter(explode('/', trim($this->currentPath, '/')));

		if ($newPath[0] === '/') { // if the input string start with "/" char we reset the current path.
			$tmpPath = [];
		}

		foreach (explode('/', trim($newPath, '/')) as $dir) {  // navigate the new path
			switch ($dir) {
				case '..': 
					// navigate up the tree
					if (count($tmpPath) > 0) {
						array_pop($tmpPath);
					}
					break;
				default:
					// navigate down the tree
					$tmpPath[] = $dir;
					break;
			}
		}
		// set the new internal path
		$this->currentPath = '/' . implode('/', $tmpPath);
		return $this;
	}

	/**
	 * Returns the current path
	 *
	 * @return string The current path
	 */
	public function getCurrentPath(): string
	{
		return $this->currentPath;
	}

}
