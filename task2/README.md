# TASK 1

**Language**: PHP

**Description**:  
Write a function that provides change directory (cd) function for an abstract file system.

Notes:
- root path is '/'.
- path separator is '/'.
- parent directory is addressable as '..'.
- directory names consist only of English alphabet letters (A-Z and a-z).
- the function will not be passed any invalid paths.
- do not use built-in path-related functions.

For example:
```
$path = new Path('/a/b/c/d');
$path->cd('../x');
echo $path->currentPath;
should display '/a/b/c/x'.
```

**How To Submit**:  
Complete the source file named `change_directory.php`.



## Notes

In this case I've implemented a class (`src/Path.php`) which provides the functionality.  
The file `change_directory.php` uses this class.

The class has the construct, in order to initialize the object but also two methods:
- `cd`: used to navigate a new path
- `getCurrentPath`: to get the current path

You need PHP >= 7.0 in order to run the script.

### Test

In this case, I've also provided a Test class in order to test several scenarios.

To execute the tests, run `composer install` to install the dependencies, and then run `vendor/bin/phpunit UnitTestFiles/PathTest.php`.

