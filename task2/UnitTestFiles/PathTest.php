<?php

namespace UnitTestFiles;

use PHPUnit\Framework\TestCase;
use Task2\Path;

class PathTest extends TestCase
{

	/**
	 * Test several path changes
	 */
	public function testCd(): void
	{
		$mapTest = [
			new MapTest('up one level, then inside x','/a/b/c/d', ['../x'], '/a/b/c/x'),
			new MapTest('reset','/a/b/c/d', ['/abd'], '/abd'),
			new MapTest('only up','/a/b/c/d', ['../..'], '/a/b'),
			new MapTest('only up, to the root','/a', ['../..'], '/'),
			new MapTest('only down','/', ['a/b/c'], '/a/b/c'),
		];

		foreach ($mapTest as $walk) {
			$p = new Path($walk->initial);
			foreach ($walk->steps as $step) {
				$p->cd($step);
			}
			self::assertEquals($walk->result, $p->getCurrentPath(), $walk->testName);
		}
	}

}

class MapTest {
	public $initial;
	public $steps = [];
	public $result;
	public $testName;

	public function __construct(string $testName, string $initial, array $steps, string $result)
	{
		$this->initial = $initial;
		$this->steps = $steps;
		$this->result = $result;
		$this->testName = $testName;
	}

}
