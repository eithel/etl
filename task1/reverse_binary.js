/**
 * ReverseNumbers takes number and returns its byte presentation reverted.
 * @param  {...any} numbers list of numbers to convert
 * @returns {Array<String>} list of numbers in byte presentation reverted
 */
function ReverseNumbers(...numbers) {
    return numbers.map((e) => {
        const n = parseInt(e)
        if (isNaN(n)) {
            throw `unable to parse value "${e}"`;
        }

        return n
            .toString(2) // we convert in binary
            .split("")
            .reverse()
            .join("");
    });
}


// takes arguments from the CLI
var args = process.argv.slice(2);

console.log(ReverseNumbers(...args));
