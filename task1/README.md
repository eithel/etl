# TASK 1

**Language**: Javascript

**Description**:  
Write a function for reversing numbers in binary. For instance, the binary representation of 13 is 1101, and reversing it gives 1011, which corresponds to number 11.

**How to submit**:  
Complete the source code file named `reverse_binary.js`.


## Notes

Reading the task description seems like that a function should take an int as input and returns the its byte presentation reverted.

Due the fact no scope was provided in the description I've implemented it for using the function via CLI.

The function in my case receives an array of strings, and for each string tries to convert to int and then reverts its byte presentation.  
This is done to have more flexibility (the function can accept more parameters) and accepting strings fits CLI example used for demonstration.

You can run the program using `node reverse_binary.js 13`, It prints 1011.

